package stsatlantis.circe.events

import io.circe.Json
import org.scalatest.{FlatSpec, Matchers}
import io.circe.syntax._

class EventsSpec extends FlatSpec with Matchers {

  "NameEvent" should "contain eventType" in {
val name = "Bobby"

    val bobby = NameEvent(name)

    val bobbyJson = Json.obj(
      "name" -> Json.fromString(name),
      "type" -> Json.fromString("name")
    )


    bobby.asJson shouldBe bobbyJson

  }

  "AgeEvent" should "contain eventType" in {

    val age = 12
    val `12` = AgeEvent(age)

    val bobbyJson = Json.obj(
      "age" -> Json.fromInt(age),
      "type" -> Json.fromString("age")
    )

    `12`.asJson shouldBe bobbyJson

  }

  "HeightEvent" should "contain eventType" in {

    val height = 19
    val unit = "cm"
    val heightEvent = HeightEvent(height, unit)

    val bobbyJson = Json.obj(
      "height" -> Json.fromInt(height),
      "unit" -> Json.fromString(unit),
      "type" -> Json.fromString("height")
    )

    heightEvent.asJson shouldBe bobbyJson

  }


}
