package stsatlantis.circe.events

sealed abstract class Event(override val `type`: String) extends Evnt

sealed trait Evnt extends Product with Serializable {
  val `type`: String
}

final case class NameEvent(name: String) extends Event("name")
final case class AgeEvent(age: Int) extends Event("age")
final case class HeightEvent(height: Long, unit: String) extends Event("height")