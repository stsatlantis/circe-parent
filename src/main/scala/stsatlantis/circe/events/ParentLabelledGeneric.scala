package stsatlantis.circe.events

trait ParentLabelledGeneric[-T] {
  type Repr

  def to(t: T): Repr
}

object ParentLabelledGeneric {
  type Aux[T, Repr0] = ParentLabelledGeneric[T] {type Repr = Repr0}

}