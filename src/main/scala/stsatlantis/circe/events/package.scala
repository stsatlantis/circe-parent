package stsatlantis.circe

import io.circe.Encoder
import io.circe.generic.parent._
import shapeless._
import shapeless.labelled.FieldType
import shapeless.ops.record.{Merger, Remover}
import shapeless.syntax.singleton._
import shapeless.tag.@@


package object events {

  implicit val eventLGen: ParentLabelledGeneric.Aux[Event, FieldType[Symbol @@ Witness.`"type"`.T, String] :: HNil] = new ParentLabelledGeneric[Event] {
    override type Repr = FieldType[Symbol @@ Witness.`"type"`.T, String] :: HNil

    override def to(t: Event): Repr = ('type ->> t.`type`) :: HNil

  }

  implicit def materializeProduct[
  P,
  K,
  EL <: HList,
  T <: P,
  V <: HList,
  RO <: HList](
    implicit
    gen: LabelledGeneric.Aux[T, V],
    evGen: ParentLabelledGeneric.Aux[P, EL],
    ev: K <:< Witness.`"name"`.T,
    merger: Merger.Aux[EL, V, RO],
    remover: Remover.Aux[RO, K, V]
  ): LabelledGeneric.Aux[T, RO] =
    new LabelledGeneric[T] {
      type Repr = RO

      def to(t: T): Repr = merger(evGen.to(t), gen.to(t))

      override def from(r: RO): T = gen.from(remover(r))
    }

  implicit val nameEventEncoder: Encoder[NameEvent] = deriveEncoder[NameEvent]
  implicit val ageEventEncoder: Encoder[AgeEvent] = deriveEncoder[AgeEvent]
  implicit val heightEventEncoder: Encoder[HeightEvent] = deriveEncoder[HeightEvent]

}
