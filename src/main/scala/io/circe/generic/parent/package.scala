package io.circe.generic

import io.circe.generic.decoding.DerivedDecoder
import io.circe.generic.encoding.ReprObjectEncoder
import io.circe.{Decoder, JsonObject, ObjectEncoder}
import shapeless.ops.record.Merger
import shapeless.{HList, LabelledGeneric, Lazy}
import stsatlantis.circe.events.ParentLabelledGeneric

package object parent {
  final def deriveDecoder[A](implicit decode: Lazy[DerivedDecoder[A]]): Decoder[A] = decode.value

  final def deriveEncoder[A](implicit encode: Lazy[ParentObjectEncoder[A]]): ObjectEncoder[A] = encode.value
}

abstract class ParentObjectEncoder[A] extends ObjectEncoder[A]

final object ParentObjectEncoder {

  implicit def deriveEncoder[A <: P, R <: HList, P, PR <: HList, Out <: HList](implicit
    gen: LabelledGeneric.Aux[A, R],
    lgen: ParentLabelledGeneric.Aux[P, PR],
    merger: Merger.Aux[PR, R, Out],
    encode: Lazy[ReprObjectEncoder[Out]]
  ): ParentObjectEncoder[A] = new ParentObjectEncoder[A] {
    final def encodeObject(a: A): JsonObject = encode.value.encodeObject(merger(lgen.to(a), gen.to(a)))
  }

}