name := "circe-extension"

version := "0.1"

scalaVersion := "2.12.6"

val CirceVersion = "0.9.3"

val Circe = Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser",
  "io.circe" %% "circe-generic-extras",
).map(_ % CirceVersion)

val dependencies = Circe

val testDependencies = {
  List("org.scalatest" %% "scalatest" % "3.0.4")
}.map(_ % Test)

libraryDependencies ++= dependencies ++ testDependencies